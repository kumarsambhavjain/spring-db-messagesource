/**
 * 
 */
package com.samsoft.dbmessage.test;

import java.util.Locale;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author Kumar Sambhav Jain
 * 
 *         INSERT INTO MESSAGES (CODE,LOCALE,MESSAGE) VALUES
 *         ('sample.code','en_US','This is amazing !!!!');
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext-dbmsg.xml" })
public class DatabaseMessageSourceTest {

	@Autowired(required = true)
	private ApplicationContext applicationContext;


	@Test
	public void testCtxMessage() {
		String message = applicationContext.getMessage("sample.code",
				new Object[] { "" }, Locale.US);
		Assert.assertTrue(message.equals("This is amazing  !!!!"));
	}

}
