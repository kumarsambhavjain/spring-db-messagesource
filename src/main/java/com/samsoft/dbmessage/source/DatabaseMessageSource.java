/**
 * 
 */
package com.samsoft.dbmessage.source;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceResolvable;
import org.springframework.context.NoSuchMessageException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

/**
 * @author Kumar Sambhav Jain
 */
public class DatabaseMessageSource implements MessageSource {

	private DataSource dataSource;
	private NamedParameterJdbcTemplate jdbcTemplate;

	/**
	 * @param dataSource
	 */
	public DatabaseMessageSource(DataSource dataSource) {
		super();
		this.dataSource = dataSource;
		jdbcTemplate = new NamedParameterJdbcTemplate(this.dataSource);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.context.MessageSource#getMessage(java.lang.String,
	 * java.lang.Object[], java.lang.String, java.util.Locale)
	 */
	@Override
	public String getMessage(String code, Object[] args, String defaultMessage,
			Locale locale) {
		String message = getMessage(code, args, locale);
		if (message == null)
			return defaultMessage;
		else
			return message;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.context.MessageSource#getMessage(java.lang.String,
	 * java.lang.Object[], java.util.Locale)
	 */
	@Override
	public String getMessage(String code, Object[] args, Locale locale)
			throws NoSuchMessageException {
		Map<String, String> paramMap = new HashMap<String, String>();
		paramMap.put("code", code);
		paramMap.put("locale", locale.toString());
		String queryForObject = jdbcTemplate
				.queryForObject(
						"SELECT MESSAGE FROM MESSAGES WHERE CODE=:code AND LOCALE=:locale ",
						paramMap, String.class);
		String result = MessageFormat.format(queryForObject, args);
		if (result == null)
			throw new NoSuchMessageException(code, locale);
		else
			return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.context.MessageSource#getMessage(org.springframework
	 * .context.MessageSourceResolvable, java.util.Locale)
	 */
	@Override
	public String getMessage(MessageSourceResolvable resolvable, Locale locale)
			throws NoSuchMessageException {
		Object[] arguments = resolvable.getArguments();
		String[] codes = resolvable.getCodes();
		String defaultMessage = resolvable.getDefaultMessage();
		String result = null;
		for (String code : codes) {
			result = getMessage(code, arguments, defaultMessage, locale);
			if (result != null)
				return result;
		}
		return defaultMessage;
	}

}