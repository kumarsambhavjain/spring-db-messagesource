CREATE TABLE MESSAGES(
	CODE VARCHAR(50) NOT NULL UNIQUE,
	LOCALE VARCHAR(7) NOT NULL,
	MESSAGE VARCHAR(300) NOT NULL
);

INSERT INTO MESSAGES (CODE,LOCALE,MESSAGE) VALUES ('sample.code','en_US','This is amazing {0} !!!!');